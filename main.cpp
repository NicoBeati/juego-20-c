#include <iostream>
#include <cstdlib>
#include <cstring>
#include <conio.h>

using namespace std;

#include "Funciones.h"
#include "rlutil.h"

using namespace rlutil;

int main(){
    int opc, oculto=0, colorFondo=0, puntajeMax=0 ,puntajeDesafioMax=1000000;
    char nombreMax[30], nombreDesafioMax[30] = "Apostador_Legendario";

    setBackgroundColor(GREY);
    setColor(BLACK);
    system("cls");
    recuadro(4,2,45,6);
    gotoxy(6,5);
    cout<<"\t"<<"\t"<<"BIENVENIDOS A 20!"<<endl<<endl<<endl<<endl;
    system("pause");
    while(true){
        if(colorFondo==0){setBackgroundColor(GREY);
                          setColor(BLACK);}
        else {setBackgroundColor(BLACK);
              setColor(LIGHTGREEN);}
        system("cls");
        gotoxy(7,3);
        cout<<"\t"<<"Menu del Juego"<<endl;
        linea(5,4,2);
        cout<<"\t"<<"1. Juego nuevo para un jugador"<<endl;
        cout<<"\t"<<"2. Juego nuevo para dos jugador"<<endl;
        cout<<"\t"<<"3. Mostrar puntuacion mas alta"<<endl;
        cout<<"\t"<<"4. Modo simulado"<<endl;
        cout<<"\t"<<"5. Reglamento"<<endl;
        if(colorFondo==0){cout<<"\t"<<"6. Cambiar a modo oscuro"<<endl;}
        else{cout<<"\t"<<"6. Cambiar a modo claro"<<endl;}
        if(oculto==1){cout<<"\t"<<"7. Modo desafio sin fin"<<endl;}
        cout<<"\t"<<"0. Salir del juego"<<endl<<endl;
        linea(5,15,2);
        cout<<"\t"<<"Elija una opcion: ";
        cin>>opc;
        system("cls");
        switch(opc){
            case 1: puntajeMax=unJugador(puntajeMax, nombreMax, 0);
                    break;
            case 2: puntajeMax=dosJugadores(puntajeMax, nombreMax);
                    break;
            case 3: maxPuntaje(puntajeMax,puntajeDesafioMax, oculto, nombreMax, nombreDesafioMax);
                    break;
            case 4: puntajeMax=unJugador(puntajeMax, nombreMax, 1);
                    break;
            case 5: reglamento();
                    break;
            case 6: if (colorFondo==0){colorFondo=1;}
                    else {colorFondo=0;}
                    break;
            case 7: if(oculto==1 ){puntajeDesafioMax=modoDesafio(puntajeDesafioMax, nombreDesafioMax); }
                    break;
            case 0: recuadro(4,2,50,6);
                    gotoxy(6,5);
                    cout<<"\t"<<"\t"<<"GRACIAS POR JUGAR 20!"<<endl<<endl<<endl<<endl;
                    system("pause");
                    return 0;
                    break;
            case 4444: oculto=1;
                    recuadro(4,2,50,6);
                    gotoxy(6,5);
                    cout<<" Clave aceptada. Modo Desafio desbloqueado."<<endl<<endl<<endl<<endl;
                    system("pause");
                    break;

            }


        }

    return 0;
}
