#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <conio.h>

using namespace std;
#include "rlutil.h"



/// Ingresar nombre de Jugador
void ingresoNombre(char nombre[], int jugador) {
    cout << endl << " Ingrese el nombre del jugador " << jugador << ": ";
    cin >> nombre;
    cout << endl;
}

/// Cargar numeros aleatorios en dados

void cargarAleatorio(int v[], int tam, int limite){
    int i;
    srand(time(NULL));
    for( i=0; i<tam; i++ ){
        v[i]=(rand()%limite)+1;
    }
}

/// Devolver numero mayor para tiradas

int maximoVector(int v[], int tam){
    int i, posMax=0;
    for(i=1;i<tam;i++){
        if(v[i]>v[posMax]) {
                posMax=i;
        }
    }
    return v[posMax];
}


void gotoxy(int x , int y);

void linea(int x0, int y0, int lineas){
            int z;
            gotoxy(x0,y0);
            for(z=1;z<80;z++)cout<<(char)205;
            for(z=1;z<lineas;z++)cout<<endl<<endl;
}

void recuadro(int iniX, int iniY, int ancho, int alto){
    int i, j;
    for(i=iniX; i<=iniX+ancho; i++){
        for(j=iniY; j<=iniY+alto; j++){
            gotoxy(i, j);

            //Arriba izquierda
            if(i==iniX && j==iniY){
                cout << (char) 201;
            }
            //Arriba derecha
            else if(i==iniX+ancho && j==iniY){
                cout << (char) 187;
            }
            //Abajo izquierda
            else if(i==iniX && j==iniY+alto){
                cout << (char) 200;
            }
            //Abajo derecha
            else if(i==iniX+ancho && j==iniY+alto){
                cout << (char) 188;
            }
            //Lineas arriba y abajo
            else if(j==iniY || j==iniY+alto){
                cout << (char) 205;
            }
            //Lineas izquierda y derecha
            else if(i==iniX || i==iniX+ancho){
                cout << (char) 186;
            }
            //Dentro del recuadro
            else{
                cout << "";
            }
        }
    }
}


// Dados dibujados

void dadoUno(int p, int q) {
    gotoxy(p, q);
    cout << (char) 254;
}

void dadoDos(int p, int q){
    gotoxy(p,q);
    cout<<(char)254;
    gotoxy(p+4,q+2);
    cout<<(char)254;
}

void dadoTres(int p, int q){
    gotoxy(p,q);
    cout<<(char) 254;
    gotoxy(p+4,q+2);
    cout<<(char) 254;
    gotoxy(p+2,q+1);
    cout<<(char) 254;
}

void dadoCuatro(int p, int q) {
    gotoxy(p, q);
    cout << (char) 254;
    gotoxy(p+4, q);
    cout << (char) 254;
    gotoxy(p, q+2);
    cout << (char) 254;
    gotoxy(p+4, q+2);
    cout << (char) 254;
}

void dadoCinco(int p, int q) {
    gotoxy(p, q);
    cout << (char) 254;
    gotoxy(p+4, q);
    cout << (char) 254;
    gotoxy(p, q+2);
    cout << (char) 254;
    gotoxy(p+4, q+2);
    cout << (char) 254;
    gotoxy(p+2, q+1);
    cout << (char) 254;
}

void dadoSeis(int p, int q) {
    gotoxy(p, q);
    cout << (char) 254;
    gotoxy(p+2, q);
    cout << (char) 254;
    gotoxy(p+4, q);
    cout << (char) 254;
    gotoxy(p, q+2);
    cout << (char) 254;
    gotoxy(p+2, q+2);
    cout << (char) 254;
    gotoxy(p+4, q+2);
    cout << (char) 254;
}

void mostrarLadoDado(int recuX, int vecDado){
recuadro(recuX, 6, 8, 4);
if (vecDado == 1) {
            dadoUno(recuX + 4, 8);
            }
if (vecDado == 2) {
            dadoDos(recuX + 2, 7);
            }
if (vecDado == 3) {
            dadoTres(recuX + 2, 7);
            }
if (vecDado == 4) {
            dadoCuatro(recuX + 2, 7);
            }
if (vecDado == 5) {
            dadoCinco(recuX + 2, 7);
            }
if (vecDado == 6) {
            dadoSeis(recuX + 2, 7);
            }
}




///1. MODO 1 JUGADOR Y MODO SIMULADO -------------
int unJugador(int puntajeMax, char* nombreMax, int modo) {

            int puntajeInicial = 100, cantDados = 5, sumaDados = 0, numGen = 0, apostarPuntos, puntosRonda = 0, rondasCero = 0, rondasPerdidas = 0, rondas, vecDado[5], recuX=2;
            int i, j, k, m;
            char nombre1[30];
            system("cls");
            linea(1,1,1);
            ingresoNombre(nombre1, 1);
            system("cls");
            linea(1,1,1);
            cout << endl <<" Ingrese cantidad de rondas: ";
            cin >> rondas;
            while (rondas <= 0) {
                    cout << " Valor incorrecto. Por favor ingrese una cantidad de 1 o mas rondas: ";
                    cin >> rondas;
                    }
            system("cls");
            for (i=0;i<rondas;i++) {
                    linea(1,1,1);
                    cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                    cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                    cout << " Puntos a apostar: ";
                    cin >> apostarPuntos;
                    while (apostarPuntos <= 0 || apostarPuntos > puntajeInicial) {
                            cout << " Valor incorrecto. Por favor ingrese una cantidad de puntos valida: ";
                            cin >> apostarPuntos;
                            }
                    for (j=0;j<5;j++) {
                            system("cls");
                            linea(1,1,1);
                            cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                            cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                            cout << " Tirada numero "<<j+1<<": "<<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                            if (modo==0) {cargarAleatorio(vecDado, cantDados-j, 6);}
                            else {for (m=0;m<cantDados-j;m++) {
                                    cout << " Ingrese valor del dado " << m+1 << ": ";
                                    cin >> vecDado[m];
                                    while (vecDado[m] < 1 || vecDado[m] > 6) {
                                            cout << " Valor incorrecto. Por favor ingrese un numero entre el 1 y el 6: ";
                                            cin >> vecDado[m];
                                            }
                                    }
                                    system("cls");
                                    linea(1,1,1);
                                    cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                                    cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                                    cout << " Tirada numero "<<j+1<<": "<<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                                    }
                            for (k=0;k<cantDados-j;k++) {
                                    recuadro(recuX, 6, 8, 4);
                                    mostrarLadoDado (recuX, vecDado[k]);
                                    recuX+=10;
                                    }
                            recuX = 2;
                            sumaDados+=maximoVector(vecDado, cantDados-j);
                            gotoxy(2,12);
                            system("pause");
                            system("cls");
                            }
                    if (sumaDados == 20) {
                            numGen = 1;
                            }
                    else if (sumaDados == 21) {
                            numGen = 2;
                            }
                    else if (sumaDados == 22) {
                            numGen = 3;
                            }
                    else if (sumaDados == 23) {
                            numGen = 4;
                            }
                    else if (sumaDados == 24) {
                            numGen = 5;
                            }
                    else if (sumaDados >=25) {
                            numGen = 6;
                            }
                    else { linea(1,1,1);
                            cout << endl << " Lo siento, no llegaste a 20. Has perdido " << apostarPuntos << " puntos." << endl << endl;
                            puntajeInicial-=apostarPuntos;
                            rondasPerdidas+=1;
                            system("Pause");
                            system("cls");
                            }
                    if (numGen!=0) {
                            linea(1,1,1);
                            cout << endl << " El numero generador de puntos es: " << numGen <<endl<<endl;
                            system("pause");
                            system("cls");
                            linea(1,1,1);
                            cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                            cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                            cout << " Numero generador de puntos: " << numGen << "\t"<<"\t"<< "Dados maximos acumulados: "<< sumaDados <<" /20"<< endl;
                            if (modo==0) cargarAleatorio(vecDado, cantDados, 6);
                            else {for (m=0;m<5;m++) {
                                cout << " Ingrese valor del dado " << m+1 << ": ";
                                cin >> vecDado[m];
                                while (vecDado[m] < 1 || vecDado[m] > 6) {
                                    cout << " Valor incorrecto. Por favor ingrese un numero entre el 1 y el 6: ";
                                    cin >> vecDado[m];
                                    }
                                }
                                system("cls");
                                linea(1,1,1);
                                cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                                cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                                cout << " Tirada numero "<<j+1<<": "<<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                                }
                            for (k=0;k<cantDados;k++) {
                                    recuadro(recuX, 6, 8, 4);
                                    mostrarLadoDado (recuX, vecDado[k]);
                                    recuX+=10;
                                    }
                            recuX = 2;
                            gotoxy(2,12);
                            system("pause");
                            system("cls");
                            for (k=0;k<cantDados;k++) {
                                    if (vecDado[k] == numGen) {
                                            puntajeInicial+=apostarPuntos;
                                            puntosRonda+=apostarPuntos;
                                            }
                                    }
                            linea(1,1,1);
                            cout << endl << " Conseguiste " << puntosRonda << " puntos."<< endl << endl;
                            if (puntosRonda == 0){rondasCero+=1;}
                            system("pause");
                            system("cls");
                            }
                    puntosRonda = 0;
                    sumaDados = 0;
                    numGen = 0;
                    if (puntajeInicial == 0) {
                            system("cls");
                            recuadro(4,2,80,7);
                            gotoxy(6,4);
                            cout << " Lo siento "<<nombre1<<", te quedaste sin puntos. FIN DEL JUEGO." << endl;
                            gotoxy(6,5);
                            cout << " Rondas perdidas: " << rondasPerdidas << endl;
                            gotoxy(6,6);
                            cout << " Rondas con 0 puntos: " << rondasCero << endl << endl << endl << endl;
                            system("pause");
                            system("cls");
                            i=rondas;
                            }
                    system("cls");
                    }

        /// FIN DE RONDAS  ------------------------

                    system("cls");
                    if (puntajeInicial != 0) {
                            recuadro(4,2,80,7);
                            gotoxy(6,4);
                            cout << nombre1 << " ha terminado la partida con " << puntajeInicial << " puntos." << endl;
                            gotoxy(6,5);
                            cout << "Rondas perdidas: " << rondasPerdidas << endl;
                            gotoxy(6,6);
                            cout << "Rondas con 0 puntos: " << rondasCero << endl << endl << endl << endl;
                            if (puntajeInicial > puntajeMax) {
                                    system("pause");
                                    system("cls");
                                    if(modo==0) { puntajeMax = puntajeInicial;
                                    memcpy(nombreMax, nombre1, 30);
                                    recuadro(4,2,80,7);
                                    gotoxy(6,5);
                                    cout << " FELICIDADES " << nombre1 <<"! Nueva puntuacion maxima!" << endl<< endl << endl << endl << endl;
                                    }
                                    else {
                                            recuadro(4,2,70,6);
                                            gotoxy(6,5);
                                            cout << " Conseguiste un puntaje muy alto! Lastima que es el modo Simulado."<<endl;
                                            gotoxy(6,6);
                                            cout << " Esperemos logres un puntaje similar o mejor en los otros modos." << endl << endl << endl << endl;
                                            }
                                    }
                            system("pause");
                            system("cls");
                            }

return puntajeMax;
}


///2. MODO 2 JUGADORES---------------------

int dosJugadores (int puntajeMax, char* nombreMax){

            int puntajeInicial = 100, puntajeInicial2 = 100, cantDados = 5, sumaDados = 0, numGen = 0, apostarPuntos, puntosRonda = 0, rondas, vecDado[5], recuX=2;
            int i, j, k;
            char nombre1[30], nombre2[30];
            system("cls");
            linea(1,1,1);
            ingresoNombre(nombre1, 1);
            system("cls");
            linea(1,1,1);
            ingresoNombre(nombre2, 2);
            system("cls");
            linea(1,1,1);
            cout << endl << " Ingrese cantidad de rondas: ";
            cin >> rondas;
            while (rondas <= 0) {
                    cout << " Valor incorrecto. Por favor ingrese una cantidad de 1 o mayor rondas: ";
                    cin >> rondas;
                    }
            system("cls");
            for (i=0;i<rondas;i++) {
                        sumaDados = 0;
                        linea(1,1,1);
                        cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                        cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                        cout << " Puntos a apostar: ";
                        cin >> apostarPuntos;
                        while (apostarPuntos <= 0 || apostarPuntos > puntajeInicial) {
                                cout << " Por favor ingrese una cantidad de puntos valida: ";
                                cin >> apostarPuntos;
                                }
                        for (j=0;j<5;j++) {
                                system("cls");
                                linea(1,1,1);
                                cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                                cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                                cout << " Tirada numero "<<j+1<<": "<<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                                cargarAleatorio(vecDado, cantDados-j, 6);
                                for (k=0;k<cantDados-j;k++) {
                                        recuadro(recuX, 6, 8, 4);
                                        mostrarLadoDado (recuX, vecDado[k]);
                                        recuX+=10;
                                        }
                                recuX = 2;
                                sumaDados+=maximoVector(vecDado, cantDados-j);
                                gotoxy(2,12);
                                system("pause");
                                system("cls");
                                }
                        if (sumaDados == 20) {
                                numGen = 1;
                                }
                        else if (sumaDados == 21) {
                                numGen = 2;
                                }
                        else if (sumaDados == 22) {
                                numGen = 3;
                                }
                        else if (sumaDados == 23) {
                                numGen = 4;
                                }
                        else if (sumaDados == 24) {
                                numGen = 5;
                                }
                        else if (sumaDados >=25) {
                                numGen = 6;
                                }
                        else {  linea(1,1,1);
                                cout << endl << " Lo siento "<<nombre1<<", no llegaste a 20. Has perdido " << apostarPuntos << " puntos." << endl << endl;
                                puntajeInicial-=apostarPuntos;
                                system("Pause");
                                system("cls");
                                }
                        if (numGen!=0) {
                                linea(1,1,1);
                                cout << endl << " El numero generador de puntos de " << nombre1 << " es: " << numGen <<endl;
                                system("pause");
                                system("cls");
                                linea(1,1,1);
                                cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                                cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                                cout << " Numero generador de puntos: " << numGen << "\t"<<"\t"<< "Dados maximos acumulados: "<< sumaDados <<" /20"<< endl;
                                cargarAleatorio(vecDado, cantDados, 6);
                                for (k=0;k<cantDados;k++) {
                                        recuadro(recuX, 6, 8, 4);
                                        mostrarLadoDado (recuX, vecDado[k]);
                                        recuX+=10;
                                        }
                                recuX = 2;
                                gotoxy(2,12);
                                system("pause");
                                system("cls");
                                for (k=0;k<cantDados;k++) {
                                        if (vecDado[k] == numGen) {
                                                puntajeInicial+=apostarPuntos;
                                                puntosRonda+=apostarPuntos;
                                                }
                                        }
                                linea(1,1,1);
                                cout << endl << " " << nombre1 << " consiguio " << puntosRonda << " puntos." << endl << endl;
                                system("pause");
                                }
                        puntosRonda = 0;
                        sumaDados = 0;
                        numGen = 0;

            ///JUGADOR 2 ------------------------------

                        system("cls");
                        linea(1,1,1);
                        cout << endl << " Jugador: " << nombre2 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                        cout << " Puntaje actual: " << puntajeInicial2 <<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                        cout << " Puntos a apostar: ";
                        cin >> apostarPuntos ;
                        while (apostarPuntos <= 0 || apostarPuntos > puntajeInicial2) {
                                cout << " Por favor ingrese una cantidad de puntos valida: ";
                                cin >> apostarPuntos;
                                }
                        for (j=0;j<5;j++) {
                                system("cls");
                                linea(1,1,1);
                                cout << endl << " Jugador: " << nombre2 <<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                                cout << " Puntaje actual: " << puntajeInicial2 <<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                                cout << " Tirada numero "<<j+1<<": "<<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                                cargarAleatorio(vecDado, cantDados-j, 6);
                                for (k=0;k<cantDados-j;k++) {
                                            recuadro(recuX, 6, 8, 4);
                                            mostrarLadoDado (recuX, vecDado[k]);
                                            recuX+=10;
                                            }
                                recuX = 2;
                                sumaDados+=maximoVector(vecDado, cantDados-j);
                                gotoxy(2,12);
                                system("pause");
                                system("cls");
                                }
                        if (sumaDados == 20) {
                                numGen = 1;
                                }
                        else if (sumaDados == 21) {
                                numGen = 2;
                                }
                        else if (sumaDados == 22) {
                                numGen = 3;
                                }
                        else if (sumaDados == 23) {
                                numGen = 4;
                                }
                        else if (sumaDados == 24) {
                                numGen = 5;
                                }
                        else if (sumaDados >=25) {
                                numGen = 6;
                                }
                        else {  linea(1,1,1);
                                cout << endl << " Lo siento "<<nombre2<<", no llegaste a 20. Has perdido " << apostarPuntos << " puntos." << endl << endl;
                                puntajeInicial2-=apostarPuntos;
                                system("Pause");
                                system("cls");
                                }
                        if (numGen!=0) {
                                system("cls");
                                linea(1,1,1);
                                cout << endl << " El numero generador de puntos de " << nombre2 << " es: " << numGen <<endl;
                                system("pause");
                                system("cls");
                                linea(1,1,1);
                                cout << endl << " Jugador: " << nombre2 <<"\t"<<"\t"<<"\t"<<"\t"<< "Ronda: " << i+1 << "/" << rondas << endl;
                                cout << " Puntaje actual: " << puntajeInicial2 <<"\t"<<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                                cout << " Numero generador de puntos: " << numGen << "\t"<<"\t"<< "Dados maximos acumulados: "<< sumaDados <<" /20"<< endl;
                                cargarAleatorio(vecDado, cantDados, 6);
                                for (k=0;k<cantDados;k++) {
                                        recuadro(recuX, 6, 8, 4);
                                        mostrarLadoDado (recuX, vecDado[k]);
                                        recuX+=10;
                                        }
                                recuX = 2;
                                gotoxy(2,12);
                                system("pause");
                                system("cls");
                                for (k=0;k<cantDados;k++) {
                                        if (vecDado[k] == numGen) {
                                                puntajeInicial2+=apostarPuntos;
                                                puntosRonda+=apostarPuntos;
                                                }
                                        }
                                linea(1,1,1);
                                cout << endl << " " << nombre2 << " consiguio " << puntosRonda << " puntos." << endl << endl;
                                system("pause");
                                }
                        puntosRonda = 0;
                        sumaDados=0;
                        numGen = 0;

          /// FIN DE RONDAS  ------------------------

                        if (puntajeInicial == 0 && puntajeInicial2 == 0){
                                recuadro(4,2,70,8);
                                gotoxy(6,4);
                                cout << "Ambos jugadores se quedaron sin puntos. FIN DEL JUEGO" << endl << endl << endl << endl << endl;
                                system("pause");
                                system("cls");
                                i=rondas;
                                }
                        else if (puntajeInicial == 0) {
                                system("cls");
                                recuadro(4,2,70,8);
                                gotoxy(6,6);
                                cout << "Lo siento "<<nombre1<<", te quedaste sin puntos. FIN DEL JUEGO." << endl << endl << endl << endl << endl;
                                system("pause");
                                system("cls");
                                i=rondas;
                                }
                        else if (puntajeInicial2 == 0) {
                                system("cls");
                                recuadro(4,2,70,8);
                                gotoxy(6,6);
                                cout << "Lo siento "<<nombre1<<", te quedaste sin puntos. FIN DEL JUEGO." << endl << endl << endl << endl << endl;
                                system("pause");
                                system("cls");
                                i=rondas;
                                }
                        system("cls");
                        }
            system("cls");
            if (puntajeInicial != 0) {
                    recuadro(4,2,80,7);
                    gotoxy(6,6);
                    cout << nombre1 << " ha terminado la partida con " << puntajeInicial << " puntos." << endl << endl << endl << endl;
                    if (puntajeInicial > puntajeMax) {
                                system("pause");
                                system("cls");
                                puntajeMax = puntajeInicial;
                                memcpy(nombreMax, nombre1, 30);
                                recuadro(4,2,80,7);
                                gotoxy(6,6);
                                cout <<" FELICIDADES " << nombre1 <<"! Nueva puntuacion maxima!" << endl << endl << endl << endl;
                                }
                    }
            system("pause");
            system("cls");
            if (puntajeInicial2 != 0) {
                        recuadro(4,2,80,7);
                        gotoxy(6,6);
                        cout << nombre2 << " ha terminado la partida con " << puntajeInicial2 << " puntos." << endl << endl << endl << endl;
                        if (puntajeInicial2 > puntajeMax) {
                                system("pause");
                                system("cls");
                                puntajeMax = puntajeInicial2;
                                memcpy(nombreMax, nombre2, 30);
                                recuadro(4,2,80,7);
                                gotoxy(6,6);
                                cout <<" FELICIDADES " << nombre2 <<"! Nueva puntuacion maxima!" << endl << endl << endl << endl;
                                }
                        }
            system("pause");
            system("cls");
            if (puntajeInicial > puntajeInicial2) {
                        recuadro(4,2,70,7);
                        gotoxy(6,6);
                        cout << " Ganador: " << nombre1 << " con " << puntajeInicial << " puntos." << endl << endl << endl << endl;
                        }
            else if (puntajeInicial < puntajeInicial2) {
                        recuadro(4,2,70,7);
                        gotoxy(6,6);
                        cout << " Ganador: " << nombre2 << " con " << puntajeInicial2 << " puntos." << endl << endl << endl << endl;
                        }
            else {      recuadro(4,2,70,7);
                        gotoxy(6,6);
                        cout << " Ambos jugadores terminaron con " << puntajeInicial << ".   Es un empate!" << endl << endl << endl << endl;
                        }
            system("pause");
            system("cls");
return puntajeMax;
}

///3.PUNTACION MAXIMA--------------
void maxPuntaje(int puntajeMax, int puntajeDesafioMax, int oculto, char* nombreMax, char* nombreDesafioMax){
            system("cls");
            recuadro(4,2,90,8);
            gotoxy(6,4);
            cout << "Maxima Puntuacion:" << endl;
            if (puntajeMax != 0) {
                    gotoxy(6,6);
                    cout << nombreMax << "............" << puntajeMax << " puntos." << endl<< endl << endl << endl << endl;
                    }
            else {
                   gotoxy(6,6);
                   cout << "Todavia no hay puntuaciones registradas." << endl<< endl << endl << endl << endl;
                   }
            if (oculto==1){
                    gotoxy(6,8);
                    cout << "Maximo puntaje en Modo Desafio: " << nombreDesafioMax << "......." << puntajeDesafioMax << " puntos." << endl << endl << endl << endl;
                    }
            system("pause");
            system("cls");
}

///5. REGLAMENTO------------------------------
void reglamento (){
            system("cls");
            recuadro(4,2,100,20);
            gotoxy(16,4);
            cout << "20 es un juego de dados para uno o dos jugadores dependiendo el modo de juego." << endl;
            gotoxy(16,6);
            cout << "Este consiste en tirar los dados e ir quitando el mayor hasta tener los" << endl;
            gotoxy(6,8);
            cout << "Cinco mayores dados de sus tiradas y sumarlos. Si la suma da 20 o mas, se pasara a jugar por" << endl;
            gotoxy(6,10);
            cout << "Los puntos que previamente se apostaron. De lo contrario estos puntos se pierden automaticamente." << endl;
            gotoxy(6,12);
            cout << "Asi, segun el numero sumado, correspondera un numero generador de puntos. Por ultimo se" << endl;
            gotoxy(6,14);
            cout << "Volveran a tirar los cinco dados y por cuantos resultados iguales al numero generador" << endl;
            gotoxy(6,16);
            cout << "El jugador obtiene esa cantidad multiplicada por los puntos apostados al principio de la ronda." << endl;
            gotoxy(17,19);
            cout << "Recuerda leer atentamente si quieres disfrutar el juego en su totalidad! ;)" << endl;

            recuadro(94, 3, 8, 4);  /// ARRIBA DERECHA
            dadoCuatro(94 + 2, 4);
            recuadro(6, 3, 8, 4);   /// ARRIBA IZQUIERDA
            dadoCuatro(6 + 2, 4);
            recuadro(94, 17, 8, 4);  /// ABAJO DERECHA
            dadoCuatro(94 + 2, 18);
            recuadro(6, 17, 8, 4);   /// ABAJO IZQUIERDA
            dadoCuatro(6 + 2, 18);
            cout << endl << endl << endl;

            system("pause");

}


///7.MODO DESAFIO--------------------
int modoDesafio(int puntajeDesafioMax, char* nombreDesafioMax){

            int puntajeInicial = 100, cantDados = 5, sumaDados = 0, numGen = 0, apostarPuntos, puntosRonda = 0, rondas=1, vecDado[5], recuX=2;
            int j, k;
            char nombre1[30], continuar='Y';
            recuadro(4,2,40,7);
            gotoxy(10,6);
            cout << "Bienvenido al Modo Desafio" << endl << endl << endl << endl;
            system("pause");
            system("cls");
            linea(1,1,1);
            ingresoNombre(nombre1, 1);
            system("cls");
            while (puntajeInicial!=0 && (continuar=='Y' || continuar=='y' )) {
                    linea(1,1,1);
                    cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << rondas << endl;
                    cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                    cout << " Puntos a apostar: ";
                    cin >> apostarPuntos;
                    while (apostarPuntos <= 0 || apostarPuntos > puntajeInicial) {
                            cout << "Por favor ingrese una cantidad de puntos valida: ";
                            cin >> apostarPuntos;
                            }
                    for (j=0;j<5;j++) {
                            system("cls");
                            linea(1,1,1);
                            cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<< "Ronda: " << rondas << endl;
                            cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                            cout << " Tirada numero "<<j+1<<": "<<"\t"<<"\t"<< "Dados maximos acumulados: " << sumaDados <<" /20"<< endl;
                            cargarAleatorio(vecDado, cantDados-j, 6);
                            for (k=0;k<cantDados-j;k++) {
                                    recuadro(recuX, 6, 8, 4);
                                    mostrarLadoDado (recuX, vecDado[k]);
                                    recuX+=10;
                                    }
                            recuX = 2;
                            sumaDados+=maximoVector(vecDado, cantDados-j);
                            gotoxy(2,12);
                            system("pause");
                            system("cls");
                            }
                    if (sumaDados == 20) {
                            numGen = 1;
                            }
                    else if (sumaDados == 21) {
                            numGen = 2;
                            }
                    else if (sumaDados == 22) {
                            numGen = 3;
                            }
                    else if (sumaDados == 23) {
                            numGen = 4;
                            }
                    else if (sumaDados == 24) {
                            numGen = 5;
                            }
                    else if (sumaDados >=25) {
                            numGen = 6;
                            }
                    else {  linea(1,1,1);
                            cout << endl << " Lo siento, no llegaste a 20. Has perdido " << apostarPuntos << " puntos." << endl << endl;
                            puntajeInicial-=apostarPuntos;
                            system("Pause");
                            system("cls");
                            }
                    if (numGen!=0) {
                            linea(1,1,1);
                            cout << endl << " El numero generador de puntos es: " << numGen <<endl << endl;
                            system("pause");
                            system("cls");
                            linea(1,1,1);
                            cout << endl << " Jugador: " << nombre1 <<"\t"<<"\t"<<"\t"<<"\t"<< "Ronda: " << rondas << endl;
                            cout << " Puntaje actual: " << puntajeInicial <<"\t"<<"\t"<<"\t"<< "Puntos apostados: " << apostarPuntos << endl;
                            cout << " Numero generador de puntos: " << numGen << "\t"<<"\t"<< "Dados maximos acumulados: "<< sumaDados <<" /20"<< endl;
                            cargarAleatorio(vecDado, cantDados, 6);
                            for (k=0;k<cantDados;k++) {
                                    recuadro(recuX, 6, 8, 4);
                                    mostrarLadoDado (recuX, vecDado[k]);
                                    recuX+=10;
                                    }
                            recuX = 2;
                            gotoxy(2,12);
                            system("pause");
                            system("cls");
                            for (k=0;k<cantDados;k++) {
                                if (vecDado[k] == numGen) {
                                    puntajeInicial+=apostarPuntos;
                                    puntosRonda+=apostarPuntos;
                                    }
                                }
                            linea(1,1,1);
                            cout << endl << " Conseguiste " << puntosRonda << " puntos." << endl << endl;
                            system("pause");
                            system("cls");
                         }
                /// ---------------------------------------------- DADO MISTERIOSO
                    cargarAleatorio(vecDado, cantDados, 6);
                    if (maximoVector(vecDado, cantDados)==5 && puntajeInicial!=0 ){
                            recuadro(4,2,70,10);
                            gotoxy(6,4);
                            cout << " Has conseguido un Dado Misterioso!" << endl << endl << endl << endl << endl << endl << endl << endl << endl;
                            system("pause");
                            cargarAleatorio(vecDado, 1, 6);
                            for (k=0;k<1;k++) {
                                    recuadro(35, 6, 8, 4);
                                    mostrarLadoDado (35, vecDado[k]);
                                    }
                            gotoxy(6,10);
                            cout << endl << endl << endl;
                            system("pause");
                            system("cls");
                            recuadro(4,2,70,7);
                            gotoxy(6,4);
                            switch(vecDado[0]){
                                    case 1: cout << " 1: Oh no! Pierdes la mitad de tus puntos!" << endl;
                                            if (puntajeInicial%2==0){puntajeInicial = puntajeInicial / 2;}
                                            else {puntajeInicial = (puntajeInicial+1) / 2;}
                                            break;
                                    case 2: cout << " 2: Oh no! En lugar de ganar los puntos de esta ronda, los pierdes!" << endl << endl;
                                            puntajeInicial-=(puntosRonda*2);
                                            break;
                                    case 3: cout << " 3: Ganaste el doble de puntos esta ronda!" << endl << endl;
                                            puntajeInicial+=puntosRonda;
                                            break;
                                    case 4: cout << " 4: Ganaste el triple de puntos esta ronda!" << endl << endl;
                                            puntajeInicial+=(puntosRonda*2);
                                            break;
                                    case 5: cout << " 5: Ganaste 500 puntos extras!" << endl << endl;
                                            puntajeInicial+=500;
                                            break;
                                    case 6: cout << " 6: DUPLICAS TUS PUNTOS!!!!!" << endl << endl;
                                            puntajeInicial = puntajeInicial*2;
                                            break;
                                    }
                                    gotoxy(6,6);
                                    cout << " Tu puntaje ahora es de " << puntajeInicial << " puntos" << endl << endl << endl << endl;
                                    system("pause");
                                    system("cls");
                            }
                ///-----------------------------------------
                    puntosRonda = 0;
                    sumaDados = 0;
                    numGen = 0;
                    if (puntajeInicial == 0) {
                            system("cls");
                            recuadro(4,2,70,6);
                            gotoxy(6,5);
                            cout << "Lo siento "<<nombre1<<", te quedaste sin puntos. FIN DEL JUEGO." << endl << endl << endl << endl;
                            system("pause");
                            system("cls");
                            }
                    system("cls");

                    if (puntajeInicial != 0) {
                            if (rondas%10==0){
                                    linea(1,1,1);
                                    cout << endl<< " Desea continuar la partida?  Y/N" << endl;
                                    cin >> continuar;
                                    system("cls");
                                    }
                            }
                    rondas+=1;
                    }

        /// FIN DE RONDAS  ------------------------

            system("cls");
            if (puntajeInicial != 0) {
                    recuadro(4,2,70,6);
                    gotoxy(6,6);
                    cout << nombre1 << " ha terminado la partida con " << puntajeInicial << " puntos." << endl << endl << endl << endl;
                    if (puntajeInicial > puntajeDesafioMax) {
                            system("pause");
                            system("cls");
                            puntajeDesafioMax = puntajeInicial;
                            memcpy(nombreDesafioMax, nombre1, 30);
                            recuadro(4,2,70,6);
                            gotoxy(6,6);
                            cout << "FELICIDADES " << nombre1 <<"! Nueva puntuacion maxima!" << endl << endl << endl << endl;
                            }
                    system("pause");
                    system("cls");
                    }
return puntajeDesafioMax;
}

#endif // FUNCIONES_H_INCLUDED

